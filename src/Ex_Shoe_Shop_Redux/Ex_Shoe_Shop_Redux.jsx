import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './DataShoe'
import ListShoe from './ListShoe'

export default class Ex_Shoe_Shop_edux extends Component {
    state = {
        listShoe: dataShoe,
        cart: [],
    }

    handleAddtocart = (shoe) => {
        let cloneCart = [...this.state.cart]
        // Check cart quantity
        let index = cloneCart.findIndex(item => { return item.id == shoe.id })
        if (index == -1) {
            let newShoe = { ...shoe, soLuong: 1 }
            cloneCart.push(newShoe)
        } else {
            cloneCart[index].soLuong++
        }
        this.setState({ cart: cloneCart })
    }

    handleChangeQuantity = (id, para) => {
        let cloneCart = [...this.state.cart]
        let index = this.state.cart.findIndex(item => { return item.id == id })
        if (cloneCart[index].soLuong == 1 && para == '-1') {
            cloneCart.splice(index, 1)
        } else {
            cloneCart[index].soLuong = cloneCart[index].soLuong + para * 1
        }
        this.setState({ cart: cloneCart })
    }

    handleRemove = (id) => {
        let cloneCart = [...this.state.cart]
        let index = this.state.cart.findIndex(item => { return item.id == id })
        cloneCart.splice(index, 1)
        this.setState({ cart: cloneCart })
    }

    render() {
        // Props children console.log('this.props', this.props)
        return (
            <div className='container'>
                <h1>{this.props.children}</h1>
                <Cart />
                <ListShoe/>
            </div>
        )
    }

}
