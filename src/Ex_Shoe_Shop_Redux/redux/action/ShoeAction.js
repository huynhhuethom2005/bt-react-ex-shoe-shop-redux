import { ADD_TO_CART, CHANGE_QUANTITY, DELETE_CART } from "../constans/ShoeConstans";

// FIXME: Shortcut rxaction
export const addToCartAction = (payload) => ({
    type: ADD_TO_CART,
    payload
})

export const deleteCartAction = (payload) => ({
  type: DELETE_CART,
  payload
})

export const changeQuantity = (payload) => ({
  type: CHANGE_QUANTITY,
  payload
})


