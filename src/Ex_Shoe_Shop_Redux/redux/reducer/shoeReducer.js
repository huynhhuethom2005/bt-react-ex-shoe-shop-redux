// FIXME: Shortcut rxreducer
import { dataShoe } from '../../DataShoe'
import * as types from '../constans/ShoeConstans'
let initialValue = {
    listShoe: dataShoe,
    cart: [],
}

export const shoeReducer = (state = initialValue, action) => {
    switch (action.type) {
        case types.ADD_TO_CART: {
            let cloneCart = [...state.cart]
            // Check cart quantity
            let index = cloneCart.findIndex(item => { return item.id == action.payload.id })
            if (index == -1) {
                let newShoe = { ...action.payload, soLuong: 1 }
                cloneCart.push(newShoe)
            } else {
                cloneCart[index].soLuong++
            }
            return { ...state, cart: cloneCart }
        }

        case types.DELETE_CART: {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex(item => { return item.id == action.payload })
            cloneCart.splice(index, 1)
            return { ...state, cart: cloneCart }
        }

        case types.CHANGE_QUANTITY: {
            console.log('action :>> ', action);
            let cloneCart = [...state.cart]
            let index = state.cart.findIndex(item => { return item.id == action.payload.id })
            if (cloneCart[index].soLuong == 1 && action.payload.event == '-1') {
                cloneCart.splice(index, 1)
            } else {
                cloneCart[index].soLuong = cloneCart[index].soLuong + action.payload.event * 1
            }
            return { ...state, cart: cloneCart}
        }

        default: {
            return state
        }
    }
}
