import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addToCartAction } from './redux/action/ShoeAction'
import { ADD_TO_CART } from './redux/constans/ShoeConstans'

class ItemShoe extends Component {
    render() {
        let { name, image, price } = this.props.item
        return (
            <div className="col-3 p-3">
                <div className="card">
                    <img className="card-img-top" src={image} alt="Card image cap" />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{price} $</p>
                        <button onClick={() => { this.props.handlePushToCart(this.props.item) }} className="btn btn-primary">Add to cart</button>
                    </div>
                </div>
            </div>
        )
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handlePushToCart: (shoe) => {
            // let action = {
            //     type: ADD_TO_CART,
            //     payload: shoe
            // }
            // dispatch(action)

            dispatch(addToCartAction(shoe))
        }
    }
}

export default connect(null, mapDispatchToProps)(ItemShoe)
