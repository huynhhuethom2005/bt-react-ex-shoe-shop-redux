import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemShoe from './ItemShoe'

class ListShoe extends Component {
    render() {
        return (
            <div className='row m-3'>
                {this.props.shoeObj.map((shoe) => {
                    return <ItemShoe item={shoe} />
                })}
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        shoeObj: state.shoeReducer.listShoe
    }
}

export default connect(mapStateToProps)(ListShoe)
