import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { createStore } from 'redux';
// import { rootReducer_DemoReduxMini } from './DemoReduxMini/redux/reducer/rootReducer';
import { root_Ex_Shoe_Shop_Redux } from './Ex_Shoe_Shop_Redux/redux/reducer/root_Ex_Shoe_Shop_Redux';

const store = createStore(
    // rootReducer_DemoReduxMini,
    root_Ex_Shoe_Shop_Redux,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
        <App />
    </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
